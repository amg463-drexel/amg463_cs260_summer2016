#include <iostream>
#include <assert.h>
#include "arraystack.h"

using namespace std;

int main(void) {
	ARRAYSTACK S;

	cout << "STACK TEST \n \n";
	
	//test EMPTY and constructor
	assert(S.EMPTY() == true);

	//--------------------------------------------------
	//--------test procedures with side effects---------
	//--------------------------------------------------
	S.PUSH(5);
	assert(S.TOP() == 5);
	S.PUSH(6);
	assert(S.TOP() == 6);
	S.POP();
	assert(S.TOP() == 5);
	
	//test MAKENULL
	assert(S.EMPTY() == false);
	S.MAKENULL();
	assert(S.EMPTY() == true);

	//test error cases
	assert(S.TOP() == -1);
	S.POP();
	assert(S.TOP() == -1);

	cout << "PASSED ALL TESTS \n";

	return 0;
}
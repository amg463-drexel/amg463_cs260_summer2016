#ifndef ARRAYSTACK_H
#define ARRAYSTACK_H

#define MAXSIZE 100000

class ARRAYSTACK {
public:
	ARRAYSTACK();
	int TOP();
	void POP();
	void PUSH(int x);
	bool EMPTY();
	void MAKENULL();

private:
	int elements[MAXSIZE];
	int top;
};

#endif

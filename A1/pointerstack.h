#include "pointerlist.h"
#ifndef POINTERSTACK_H
#define POINTERSTACK_H

class POINTERSTACK {
public:
	POINTERSTACK();
	int TOP();
	void POP();
	void PUSH(int x);
	bool EMPTY();
	void MAKENULL();

private:
	POINTERLIST l;
};

#endif

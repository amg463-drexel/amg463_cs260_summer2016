#include <iostream>
#include <assert.h>
#include "arraylist.h"

using namespace std;

int main(void) {
	ARRAYLIST L;

	//setup
	L.INSERT(8,1);
	L.INSERT(9, 2);
	L.INSERT(10, 3);
	L.INSERT(11, 4);
	cout << "LIST TEST \n \n";
	
	//--------------------------------
	//----------test functions--------
	//--------------------------------
	assert(L.FIRST() == 1);
	assert(L.END() == 5);
	assert(L.RETRIEVE(3) == 10);
	assert(L.LOCATE(8) == 1);
	assert(L.NEXT(1) == 2);
	assert(L.PREVIOUS(2) == 1);

	//test function error cases
	assert(L.RETRIEVE(5) == -1);
	assert(L.LOCATE(13) == L.END());
	assert(L.NEXT(5) == -1);

	//--------------------------------------------------
	//--------test procedures with side effects---------
	//--------------------------------------------------
	L.INSERT(2, 1);
	assert(L.RETRIEVE(1) == 2);
	L.DELETE(1);
	assert(L.RETRIEVE(1) == 8);

	//test procedure error cases
	int end = L.END();
	L.INSERT(2, 10);
	assert(L.END() == end);
	L.DELETE(10);
	assert(L.END() == end);

	//test MAKENULL
	assert(L.MAKENULL() == 1);

	cout << "PASSED ALL TESTS";

	return 0;
}
#include <iostream>
#include "pointerlist.h"
using namespace std;

//constructor
POINTERLIST::POINTERLIST() {
	head.next = nullptr;
}

//return: position of first element
CELL* POINTERLIST::FIRST() {
	return &head;
}

//return: position after last element
CELL* POINTERLIST::END() {
	CELL* q = &head;
	while (q->next != nullptr) {
		q = q->next;
	}
	return q;
}

//param p: position on list
//return: element at position p
int POINTERLIST::RETRIEVE(CELL* p) {
	if (p == nullptr || p->next == nullptr) {
		return -1;
	}
	return p->next->element;
}

//param x: element to be located in list
//return: position of element x
CELL* POINTERLIST::LOCATE(int x) {
	CELL* q = &head;
	while (q->next != nullptr) {
		if (q->next->element == x) {
			return q;
		}
		q = q->next;
	}
	return q;
}

//param p: a position on list
//return: the position following p
CELL* POINTERLIST::NEXT(CELL* p) {
	if (p->next == nullptr) {
		return nullptr;
	}
	return p->next;
}

//param p: a position on list
//return: the position preceding p
CELL* POINTERLIST::PREVIOUS(CELL* p) {
	CELL* q = &head;
	while (q->next != nullptr) {
		if (q->next == p) {
			return q;
		}
		q = q->next;
	}
	return nullptr;
}

//param x: element to be inserted
//param p: position in list in which to insert x
void POINTERLIST::INSERT(int x, CELL* p) {
	CELL* inserted = new CELL();
	inserted->element = x;
	inserted->next = p->next;
	p->next = inserted;
}

//param p: position to be deleted
void POINTERLIST::DELETE(CELL* p) {
	p->next = p->next->next;
}

//return: first position of empty list
CELL* POINTERLIST::MAKENULL() {
	head.next = nullptr;
	return &head;
}

//prints list (for debugging purposes)
void POINTERLIST::PRINTLIST() {
	CELL* q = &head;
	while (q->next != nullptr) {
		cout << q->next->element << " ";
		q = q->next;
	}
}
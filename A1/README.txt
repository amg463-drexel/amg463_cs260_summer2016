Amy Gottsegen
amg463

My submission is fully functional. I created classes for each of the four required implementations, and POINTERLIST has a struct CELL defined inside it. POINTERSTACK uses POINTERLIST. In functions where a position is usually returned but the call fails, -1 or nullptr is returned, for array and pointer implementations, respectively. Aside from this, my implementations conform closely to the textbook. 
#include <iostream>
#include "pointerstack.h"
#include "pointerlist.h"
using namespace std;

//constructor
POINTERSTACK::POINTERSTACK() {
}

//return: element at top of stack
int POINTERSTACK::TOP() {
	return l.RETRIEVE(l.FIRST());
}

//removes element at top of stack
void POINTERSTACK::POP() {
	if (EMPTY()) {
		return;
	}
	l.DELETE(l.FIRST());
}

//param x: element to be inserted at top of stack
void POINTERSTACK::PUSH(int x) {
	l.INSERT(x, l.FIRST());
}

//return: true if stack contains no elements
bool POINTERSTACK::EMPTY() {
	return l.END() == l.FIRST();
}

//makes calling stack an empty stack
void POINTERSTACK::MAKENULL() {
	l.MAKENULL();
}

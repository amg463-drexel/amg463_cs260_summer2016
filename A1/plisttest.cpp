#include <iostream>
#include <assert.h>
#include "pointerlist.h"

using namespace std;

int main(void) {
	//CELL* L = initTestPointerList();
	POINTERLIST L;

	cout << "POINTERLIST TEST \n \n";
	CELL* first = L.FIRST();
	L.INSERT(5, first);
	assert(L.RETRIEVE(first) == 5);
	assert(L.RETRIEVE(first->next) == -1);
	L.INSERT(6, L.END());
	assert(L.RETRIEVE(first->next) == 6);

	L.INSERT(4, first);
	L.INSERT(3, first);
	assert(L.LOCATE(4) == L.NEXT(first));
	assert(L.NEXT(L.END()) == nullptr);
	assert(L.RETRIEVE(L.PREVIOUS(L.END())) == 6);
	L.DELETE(first);
	assert(L.RETRIEVE(L.FIRST()) == 4);
	L.MAKENULL();
	assert(L.RETRIEVE(L.FIRST()) == -1);
	
	cout << "TESTS PASSED";

	return 0;
}
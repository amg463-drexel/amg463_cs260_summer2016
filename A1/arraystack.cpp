#include <iostream>
#include "arraystack.h"
using namespace std;

//constructor
ARRAYSTACK::ARRAYSTACK() {
	top = MAXSIZE;
}

//return: element at top of stack
int ARRAYSTACK::TOP() {
	if (EMPTY()) {
		return -1;
	}
	return elements[top];
}

//removes element at top of stack
void ARRAYSTACK::POP() {
	if (EMPTY()) {
		return;
	}
	top += 1;
}

//param x: element to be inserted at top of stack
void ARRAYSTACK::PUSH(int x) {
	if (top == 1) {
		return;
	}
	top -= 1;
	elements[top] = x;
}

//return: true if stack contains no elements
bool ARRAYSTACK::EMPTY() {
	if (top >= MAXSIZE) {
		return true;
	}
	return false;
}

//makes calling stack an empty stack
void ARRAYSTACK::MAKENULL() {
	top = MAXSIZE;
}
#include <iostream>
#include "arraylist.h"
using namespace std;

//constructor
ARRAYLIST::ARRAYLIST() {
	last = 0;
}

//return: position of first element
int ARRAYLIST::FIRST() {
	return 1;
}

//return: position after last element
int ARRAYLIST::END() {
	return last + 1;
}

//param p: position on list
//return: element at position p
int ARRAYLIST::RETRIEVE(int p) {
	if (p < 1 || p > last) {
		return -1;
	}
	return elements[p];
}

//param x: element to be located in list
//return: position of element x
int ARRAYLIST::LOCATE(int x) {
	for (int q = 1; q <= last; q++) {
		if (elements[q] == x) {
			return q;
		}
	}
	return last + 1;
}

//param p: a position on list
//return: the position following p
int ARRAYLIST::NEXT(int p) {
	if (p < 1 || p > last) {
		return -1;
	}
	return p + 1;
}

//param p: a position on list
//return: the position preceding p
int ARRAYLIST::PREVIOUS(int p) {
	if (p <= 1 || p > last + 1) {
		return -1;
	}
	return p - 1;
}

//param x: element to be inserted
//param p: position in list in which to insert x
void ARRAYLIST::INSERT(int x, int p) {
	if (last > MAXSIZE) {
		return;
	}
	else if (p < 1 || p > last + 1) {
		return;
	}
	else {
		for (int q = last; q >= p; q--) {
			elements[q + 1] = elements[q];
		}
		last += 1;
		elements[p] = x;
	}
}

//param p: position to be deleted
void ARRAYLIST::DELETE(int p) {
	if (p < 1 || p > last) {
		return;
	}
	else {
		last -= 1;
		for (int q = p; q < last; q++) {
			elements[q] = elements[q + 1];
		}
	}
}

//return: first position of empty list
int ARRAYLIST::MAKENULL() {
	last = 0;
	return 1;
}

//prints list (for debugging purposes)
void ARRAYLIST::PRINTLIST() {
	for (int q = 1; q <= last; q++) {
		cout << elements[q] << " ";
	}
}
#include <iostream>
#include <time.h>
#include <list>
#include <stack>
#include "arraylist.h"
#include "arraystack.h"
#include "pointerlist.h"
#include "pointerstack.h"

using namespace std;

//inserts 9000 elements into each type of list from back and prints processing time
void insertBack(ARRAYLIST* arrlist, POINTERLIST* ptrlist, list<int>* liblist) {
	clock_t t;
	t = clock();
	for (int i = 1; i <= 9000; i++) {
		arrlist->INSERT(i, arrlist->END());
	}
	t = clock() - t;
	cout << "array list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		ptrlist->INSERT(i, ptrlist->END());
	}
	t = clock() - t;
	cout << "pointer list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		liblist->push_back(i);
	}
	t = clock() - t;
	cout << "library list: " << t << "\n";
}

//inserts 9000 elements into each type of list from front and prints processing time
void insertFront(ARRAYLIST* arrlist, POINTERLIST* ptrlist, list<int>* liblist) {
	clock_t t;
	t = clock();
	for (int i = 1; i <= 9000; i++) {
		arrlist->INSERT(i, arrlist->FIRST());
	}
	t = clock() - t;
	cout << "array list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		ptrlist->INSERT(i, ptrlist->FIRST());
	}
	t = clock() - t;
	cout << "pointer list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		liblist->push_front(i);
	}
	t = clock() - t;
	cout << "library list: " << t << "\n";
}

//traverses each type of 9000 element list and prints processing time
void traverse(ARRAYLIST* arrlist, POINTERLIST* ptrlist, list<int>* liblist) {
	clock_t t;
	int p = 1;
	t = clock();
	while(arrlist->NEXT(p) != arrlist->END()) {
		p = arrlist->NEXT(p);
	}
	t = clock() - t;
	cout << "array list: " << t << "\n";

	CELL* q = ptrlist->FIRST();
	t = clock();
	while(ptrlist->NEXT(q) != ptrlist->END()) {
		q = ptrlist->NEXT(q);
	}
	t = clock() - t;
	cout << "pointer list: " << t << "\n";

	t = clock();
	for (list<int>::iterator it = liblist->begin(); it != liblist->end(); ++it) {
		p = *it;
	}
	t = clock() - t;
	cout << "library list: " << t << "\n";
}

//deletes from front all elements of each type of 9000 element list and prints processing time
void deleteFront(ARRAYLIST* arrlist, POINTERLIST* ptrlist, list<int>* liblist) {
	clock_t t;
	t = clock();
	for (int i = 1; i <= 9000; i++) {
		arrlist->DELETE(arrlist->FIRST());
	}
	t = clock() - t;
	cout << "array list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		ptrlist->DELETE(ptrlist->FIRST());
	}
	t = clock() - t;
	cout << "pointer list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		liblist->pop_front();
	}
	t = clock() - t;
	cout << "library list: " << t << "\n";
}

//deletes from back all elements of each type of 9000 element list and prints processing time
void deleteBack(ARRAYLIST* arrlist, POINTERLIST* ptrlist, list<int>* liblist) {
	clock_t t;
	t = clock();
	for (int i = 1; i <= 9000; i++) {
		arrlist->DELETE(arrlist->PREVIOUS(arrlist->END()));
	}
	t = clock() - t;
	cout << "array list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		ptrlist->DELETE(ptrlist->PREVIOUS(ptrlist->END()));
	}
	t = clock() - t;
	cout << "pointer list: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 9000; i++) {
		liblist->pop_back();
	}
	t = clock() - t;
	cout << "library list: " << t << "\n";
}

//pushes 90,000 elements into each type of stack and prints processing time
void push(ARRAYSTACK* arrstack, POINTERSTACK* ptrstack, stack<int>* libstack) {
	clock_t t;
	t = clock();
	for (int i = 1; i <= 90000; i++) {
		arrstack->PUSH(i);
	}
	t = clock() - t;
	cout << "array stack: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 90000; i++) {
		ptrstack->PUSH(i);
	}
	t = clock() - t;
	cout << "pointer stack: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 90000; i++) {
		libstack->push(i);
	}
	t = clock() - t;
	cout << "library stack: " << t << "\n";
}

//pops 90,000 elements from each type of stack and prints processing time
void pop(ARRAYSTACK* arrstack, POINTERSTACK* ptrstack, stack<int>* libstack) {
	clock_t t;
	t = clock();
	for (int i = 1; i <= 90000; i++) {
		arrstack->POP();
	}
	t = clock() - t;
	cout << "array stack: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 90000; i++) {
		ptrstack->POP();
	}
	t = clock() - t;
	cout << "pointer stack: " << t << "\n";

	t = clock();
	for (int i = 1; i <= 90000; i++) {
		libstack->pop();
	}
	t = clock() - t;
	cout << "library stack: " << t << "\n";
}

int main(void) {
	ARRAYLIST arrlist;
	POINTERLIST ptrlist;
	list<int> liblist;
	
	cout << "LIST OPERATIONS TIMING: \n";
	cout << "Inserting to front: \n";
	insertFront(&arrlist, &ptrlist, &liblist);
	cout << "\n Traversal: \n";
	traverse(&arrlist, &ptrlist, &liblist);
	cout << "\n Deleting from front: \n";
	deleteFront(&arrlist, &ptrlist, &liblist);
	cout << "\n Inserting to back: \n";
	insertBack(&arrlist, &ptrlist, &liblist);
	cout << "\n Deleting from back: \n";
	deleteBack(&arrlist, &ptrlist, &liblist);

	ARRAYSTACK arrstack;
	POINTERSTACK ptrstack;
	stack<int> libstack;

	cout << "\n STACK OPERATIONS TIMING: \n";
	cout << "\n Push: \n";
	push(&arrstack, &ptrstack, &libstack);
	cout << "\n Pop: \n";
	pop(&arrstack, &ptrstack, &libstack);
	return 0;
}
#include <iostream>
#include <stack>

using namespace std;
int main(int argc, char *argv[]) {
	string expression = argv[1];
	cout << "EVALUTATING PREFIX EXPRESSION: " << expression << "\n";
	stack<char> exp;
	stack<int> s;
	for(char c : expression) {
		exp.push(c);
	}
	while (!exp.empty()) {
		char c = exp.top();
		if (isdigit(c)) {
			int n = c-'0';
			s.push(n);
		}
		else if (c == '+') {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(x + y);
		}
		else if (c == '-') {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(y-x);
		}
		else if (c == '*') {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(x * y);
		}
		else {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(y/x);
		}
		exp.pop();
	}
	cout << "\n RESULT: " << s.top() << "\n";
}
#ifndef ARRAYLIST_H
#define ARRAYLIST_H
#define MAXSIZEALIST 2000

class ARRAYLIST {
public:
	ARRAYLIST();
	int FIRST();
	int END();
	int RETRIEVE(int p);
	int LOCATE(int x);
	int NEXT(int p);
	int PREVIOUS(int p);
	void INSERT(int x, int p);
	void DELETE(int p);
	int MAKENULL();
	void PRINTLIST();

private:
	int elements[MAXSIZEALIST];
	int last;
};

#endif

#include <iostream>
#include <stack>

using namespace std;

int main(int argc, char *argv[]) {
	string expression = argv[1];
	cout << "EVALUTATION POSTORDER EXPRESSION: " << expression << "\n";
	stack<int> s;
	for (char c : expression) {
		if (isdigit(c)) {
			int n = c - '0';
			s.push(n);
		}
		else if (c == '+') {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(x + y);
		}
		else if (c == '-') {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(y - x);
		}
		else if (c == '*') {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(x * y);
		}
		else {
			int x = s.top();
			s.pop();
			int y = s.top();
			s.pop();
			s.push(y / x);
		}
	}
	cout << "\n RESULT: " << s.top() << "\n";

}
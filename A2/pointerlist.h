#ifndef POINTERLIST_H
#define POINTERLIST_H

using namespace std;

struct CELL {
	int element;
	CELL* next;

	CELL() {
		element = 0;
		next = nullptr;
	}
};

class POINTERLIST {
public:
	POINTERLIST();

	CELL* FIRST();
	CELL* END();
	int RETRIEVE(CELL* p);
	CELL* LOCATE(int x);
	CELL* NEXT(CELL* p);
	CELL* PREVIOUS(CELL* p);
	void INSERT(int x, CELL* p);
	void DELETE(CELL* p);
	CELL* MAKENULL();
	void PRINTLIST();

private:
	CELL head;
};

#endif
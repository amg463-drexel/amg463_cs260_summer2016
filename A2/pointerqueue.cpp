#include <iostream>
#include "pointerlist.h"
#include "pointerqueue.h"
using namespace std;

//constructor
POINTERQUEUE::POINTERQUEUE() {
}

void POINTERQUEUE::MAKENULL() {
	l.MAKENULL();
}

//return: first element
int POINTERQUEUE::FRONT() {
	return l.RETRIEVE(l.FIRST());
}

//adds element x at the end of the queue
void POINTERQUEUE::ENQUEUE(int x) {
	l.INSERT(x, l.END());
}

//removes the first element in the queue
void POINTERQUEUE::DEQUEUE() {
	l.DELETE(l.FIRST());
}

//return: true if queue contains no elements, false otherwise
bool POINTERQUEUE::EMPTY() {
	return l.RETRIEVE(l.FIRST())== -1;
}
#include <iostream>
#include "lcrstree.h"
using namespace std;

//constructor
LCRSTREE::LCRSTREE() {
	avail = 1;
}

//param node: cursor to node
//return : cursor to parent of node
int LCRSTREE::PARENT(int node) {
	int p = 1;
	int i;
	while (cellspace[p].label != 0) {
		i = cellspace[p].lchild;
		while (i != 0) {
			if (i == node) {
				return p;
			}
			i = cellspace[i].rsibling;
		}
		p++;
	}
	return 0;
}

//param node: cursor to node
//return : cursor to leftmost child of node
int LCRSTREE::LEFTMOST_CHILD(int node) {
	return cellspace[node].lchild;
}

//param node: cursor to node
//return : cursor to right sibling of node
int LCRSTREE::RIGHT_SIBLING(int node) {
	return cellspace[node].rsibling;
}

//param node: cursor to node
//return : label of node
char LCRSTREE::LABEL(int node) {
	return cellspace[node].label;
}

//param v: label for new root
//return : cursor to new root
//creates one node in the cellspace
int LCRSTREE::CREATE0(char v) {
	int p = avail;
	cellspace[p] = LCRSCELL(v,0,0);
	avail += 1;
	return p;
}

//param v: label for new root
//param tree 1: cursor to node to be added as subtree of new root
//return : cursor to new root
int LCRSTREE::CREATE1(char v, int tree1) {
	int root = CREATE0(v);
	cellspace[root].lchild = tree1;
	return root;
}

//param v: label for new root
//param tree 1, tree 2: cursors to nodes to be added as subtrees of new root
//return : cursor to new root
int LCRSTREE::CREATE2(char v, int tree1, int tree2) {
	int root = CREATE0(v);
	cellspace[root].lchild = tree1;
	cellspace[tree1].rsibling= tree2;
	return root;
}

//param v: label for new root
//param tree 1, tree 2, tree 3: cursors to nodes to be added as subtrees of new root
//return : cursor to new root
int LCRSTREE::CREATE3(char v, int tree1, int tree2, int tree3) {
	int root = CREATE0(v);
	cellspace[root].lchild = tree1;
	cellspace[tree1].rsibling = tree2;
	cellspace[tree2].rsibling = tree3;
	return root;
}

//param tree: tree to be made the null node
void LCRSTREE::MAKENULL(int tree) {
	cellspace[tree].label = 0;
	cellspace[tree].lchild = 0;
	cellspace[tree].rsibling = 0;
}

//param tree : cursor to tree
//return : cursor to root of tree
int LCRSTREE::ROOT(int tree) {
	return tree;
}

//debugging method
void LCRSTREE::PRINTNODES() {
	cout << "NODES: \n";
	int p = 1;
	while (cellspace[p].label != 0) {
		cout << cellspace[p].label << "," << cellspace[p].lchild<< "," << cellspace[p].rsibling << "\n";
		p++;
	}
}
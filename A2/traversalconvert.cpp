#include "loctree.h"
#include "arraylist.h"
#include <iostream>
#include <list>

using namespace std;

/*struct listItem {
	int element;
	bool leaf;

	listItem(int e, bool l) {
		element= e;
		leaf = l;
	}
};

int post2pre(list<listItem> l) {
	LOCTREE t;
	list<listItem>::iterator it = l.begin();
	list<listItem>::iterator prev = it;
	while (it != l.end() && it->leaf) {
		it++;
	}
	int node = t.CREATE2(it->element, prev(it), l[i - 1].element);
	return 0;
}*/

void post2pre(ARRAYLIST L) {
	LOCTREE t;
	int p = L.FIRST();
	while (L.NEXT(L.FIRST()) != L.END()) {
		//find first internal node
		while (p != L.END() && L.RETRIEVE(p) > 100) {
			p = L.NEXT(p);
		}
		//construct subtree of internal node and 2 previous leaves
		//left child
		p = L.PREVIOUS(p);
		int childL = t.CREATE0('0' + L.RETRIEVE(p) - 100);
		L.DELETE(p);

		//right child
		p = L.PREVIOUS(p);
		int childR = t.CREATE0('0' + L.RETRIEVE(p) - 100);
		L.DELETE(p);
		
		//internal node
		int nodeLabel = L.RETRIEVE(p);
		int node = t.CREATE2('0' + nodeLabel, childL, childR);
		L.DELETE(p);
		L.INSERT(nodeLabel + 100, p);
		cout << "\n-----next iteration: ";
		L.PRINTLIST();
	}
	cout << "\n the constructed tree: \n";
	t.PRINTNODES();
}

int main(void) {
	/*list<listItem> l;
	l.push_back(listItem(1, true));
	l.push_back(listItem(2, true));
	l.push_back(listItem(3, true));*/
	ARRAYLIST L;
	L.INSERT(101, 1);
	L.INSERT(103, 2);
	L.INSERT(104, 3);
	L.INSERT(2, 4);
	L.INSERT(5, 5);
	cout << "-----postorder list: ";
	L.PRINTLIST();
	post2pre(L);

	return 0;
}
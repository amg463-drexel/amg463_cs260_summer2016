#include "loctree.h"
#include "lcrstree.h"
#include <iostream>
#include <ctime>

using namespace std;

//param n: root of tree
//param t: pointer to LOCTREE which contains n
void preorderLoc(int n, LOCTREE* t) {
	cout << n << " ";
	int i = t->LEFTMOST_CHILD(n);
	while (i != 0) {
		preorderLoc(i, t);
		i = t->RIGHT_SIBLING(i);
	}
}

//param n: root of tree
//param t: pointer to LCRSTREE which contains n
void preorderLcrs(int n, LCRSTREE* t) {
	cout << n << " ";
	int i = t->LEFTMOST_CHILD(n);
	while (i != 0) {
		preorderLcrs(i, t);
		i = t->RIGHT_SIBLING(i);
	}
}

//param n: root of tree
//param t: pointer to LOCTREE which contains n
void postorderLoc(int n, LOCTREE* t) {
	cout << n << " ";
	int i = t->LEFTMOST_CHILD(n);
	while (i != 0) {
		postorderLoc(i, t);
		i = t->RIGHT_SIBLING(i);
	}
}

//param n: root of tree
//param t: pointer to LCRSTREE which contains n
void postorderLcrs(int n, LCRSTREE* t) {
	cout << n << " ";
	int i = t->LEFTMOST_CHILD(n);
	while (i != 0) {
		postorderLcrs(i, t);
		i = t->RIGHT_SIBLING(i);
	}
}

//param locRoot: root of list of children tree
//param loc: LOCTREE which contains list of children tree
//param lcrsRoot: root of leftmost-child right-sibling tree
//param lcrs: LCRSTREE which contains leftmost-child right-sibling tree
//runs timing experiments for trees of a given height
void runTime(LOCTREE* loc, int locRoot, LCRSTREE* lcrs, int lcrsRoot) {
	clock_t start, stop;
	start = clock();
	preorderLoc(locRoot, loc);
	stop = clock();
	cout << "\n-----------------TIME FOR PREORDER TRAVERSAL OF LOCTREE: ";
	cout << (1000 * ((double)stop - start) / CLOCKS_PER_SEC) << " milliseconds\n\n";

	start = clock();
	preorderLcrs(lcrsRoot, lcrs);
	stop = clock();
	cout << "\n-----------------TIME FOR PREORDER TRAVERSAL OF LCRSTREE: ";
	cout << (1000 * ((double)stop - start) / CLOCKS_PER_SEC) << " milliseconds\n\n";

	start = clock();
	postorderLoc(locRoot, loc);
	stop = clock();
	cout << "\n-----------------TIME FOR POSTORDER TRAVERSAL OF LOCTREE: ";
	cout << (1000 * ((double)stop - start) / CLOCKS_PER_SEC) << " milliseconds\n\n";

	start = clock();
	postorderLcrs(lcrsRoot, lcrs);
	stop = clock();
	cout << "\n-----------------TIME FOR POSTORDER TRAVERSAL OF LCRSTREE: ";
	cout << (1000 * ((double)stop - start) / CLOCKS_PER_SEC) << " milliseconds\n\n";
}

//creates a LOCTREE and LCRSTREE of height 3 and runs timing experiments
void createTreesH3() {
	LOCTREE loc;
	LCRSTREE lcrs;

	for (int i = 0; i < 27; i++) {
		loc.CREATE0('a');
		lcrs.CREATE0('a');
	}
	for (int i = 1; i < 10; i++) {
		loc.CREATE3('a', 3 * i - 2, 3 * i - 1, 3 * i);
		lcrs.CREATE3('a', 3 * i - 2, 3 * i - 1, 3 * i);
	}
	for (int i = 1; i < 4; i++) {
		loc.CREATE3('a', (3 * i - 2) + 27, (3 * i - 1) + 27, (3 * i) + 27);
		lcrs.CREATE3('a', (3 * i - 2) + 27, (3 * i - 1) + 27, (3 * i) + 27);
	}
	int locRoot = loc.CREATE3('a', 37, 38, 39);
	int lcrsRoot = lcrs.CREATE3('a', 37, 38, 39);

	runTime(&loc, locRoot, &lcrs, lcrsRoot);
}

//creates a LOCTREE and LCRSTREE of height 4 and runs timing experiments
void createTreesH4() {
	LOCTREE loc;
	LCRSTREE lcrs;

	for (int i = 0; i < 81; i++) {
		loc.CREATE0('a');
		lcrs.CREATE0('a');
	}
	for (int i = 1; i < 28; i++) {
		loc.CREATE3('a', 3 * i - 2, 3 * i - 1, 3 * i);
		lcrs.CREATE3('a', 3 * i - 2, 3 * i - 1, 3 * i);
	}
	for (int i = 1; i < 10; i++) {
		loc.CREATE3('a', (3 * i - 2) + 81, (3 * i - 1) + 81, (3 * i) + 81);
		lcrs.CREATE3('a', (3 * i - 2) + 81, (3 * i - 1) + 81, (3 * i) + 81);
	}
	for (int i = 1; i < 4; i++) {
		loc.CREATE3('a', (3 * i - 2) + 108, (3 * i - 1) + 108, (3 * i) + 108);
		lcrs.CREATE3('a', (3 * i - 2) + 108, (3 * i - 1) + 108, (3 * i) + 108);
	}
	int locRoot = loc.CREATE3('a', 118, 119, 120);
	int lcrsRoot = lcrs.CREATE3('a', 118, 119, 120);

	runTime(&loc, locRoot, &lcrs, lcrsRoot);
}

int main(void) {
	cout << "\n TIMING EXPERIMENTS FOR TREES : HEIGHT 3 \n";
	createTreesH3();
	cout << "\n TIMING EXPERIMENTS FOR TREES : HEIGHT 4 \n";
	createTreesH4();
	return 0;
}
#include <iostream>
#include "arraylist.h"

using namespace std;

//param L: a list of lists to concatenate
//param end: the last position in L
//return : the concatenated list
ARRAYLIST concatLists(ARRAYLIST L[], int end) {
	ARRAYLIST Q, OUT;
	for (int i = 0; i < end; i++) {
		Q = L[i];
		int p = Q.FIRST();
		while (p != Q.END()) {
			OUT.INSERT(Q.RETRIEVE(p), OUT.END());
			p++;
		}
	}
	return OUT;
}

int main(void) {
	//create 3 sample arrays
	ARRAYLIST A, B, C;
	for (int i = 1; i < 5; i++) {
		A.INSERT(i, A.END());
		B.INSERT(i + 4, B.END());
		C.INSERT(i + 8, C.END());
	}
	cout << "\n The list of lists before concatenation: \n";
	A.PRINTLIST();
	cout << "\n";
	B.PRINTLIST();
	cout << "\n";
	C.PRINTLIST();

	//create list of lists with end indicating last position
	ARRAYLIST L[] = { A,B,C };
	int end = 3;
	
	ARRAYLIST OUT = concatLists(L, end);

	cout << "\n The concatenated list: ";
	OUT.PRINTLIST();
	cout << "\n";
}
#include <iostream>
#include <assert.h>
#include "lcrstree.h"

using namespace std;

int main(void) {

	cout << "LEFTMOST-CHILD RIGHT-SIBLING TREE TEST \n \n";
	LCRSTREE t;

	//construct tree 3.31
	int m = t.CREATE0('m');
	int n = t.CREATE0('n');
	int i = t.CREATE2('i', m, n);
	int e = t.CREATE1('e', i);
	int d = t.CREATE0('d');
	int b = t.CREATE2('b', d, e);
	int j = t.CREATE0('j');
	int k = t.CREATE0('k');
	int l = t.CREATE0('l');
	int g = t.CREATE2('g', j, k);
	int h = t.CREATE1('h', l);
	int f = t.CREATE0('f');
	int c = t.CREATE3('c', f, g, h);
	int a = t.CREATE2('a', b, c);

	//test CREATE0, CREATE1 and navigation functions
	assert(m == 1);
	assert(t.LEFTMOST_CHILD(e) == i);
	assert(t.PARENT(i) == e);

	//test CREATE2 and navigation functions
	assert(t.PARENT(m) == i);
	assert(t.PARENT(n) == i);
	assert(t.RIGHT_SIBLING(m) == n);
	assert(t.LEFTMOST_CHILD(i) == m);

	//test CREATE3 and navigation functions
	assert(t.PARENT(f) == c);
	assert(t.PARENT(g) == c);
	assert(t.PARENT(h) == c);
	assert(t.RIGHT_SIBLING(f) == g);
	assert(t.RIGHT_SIBLING(g) == h);
	assert(t.LEFTMOST_CHILD(c) == f);

	//test root and label
	assert(a == t.ROOT(a));
	assert(t.LABEL(f) == 'f');

	//test error cases or navigation off tree
	assert(t.PARENT(t.PARENT(t.PARENT(e))) == 0);
	assert(t.RIGHT_SIBLING(c) == 0);
	assert(t.LEFTMOST_CHILD(d) == 0);

	//test makenull
	t.MAKENULL(a);
	assert(t.LEFTMOST_CHILD(a) == 0);

	cout << "PASSED ALL TESTS \n";

	return 0;
}

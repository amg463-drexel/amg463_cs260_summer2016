#ifndef LOCTREE_H
#define LOCTREE_H

#define MAXSIZELOC 1000

using namespace std;

struct CELL {
	int node;
	int next;

	CELL() {
		node = 0;
		next = 0;
	}

	CELL(int e, int n) {
		node = e;
		next = n;
	}
};

struct NODE {
	char label;
	int header;

	NODE() {
		label = 0;
		header = 0;
	}

	NODE(char v, int h) {
		label = v;
		header = h;
	}
};

class LOCTREE {
public:
	LOCTREE();
	int PARENT(int node);
	int LEFTMOST_CHILD(int node);
	int RIGHT_SIBLING(int node);
	char LABEL(int node);
	int CREATE0(char v);
	int CREATE1(char v, int tree1);
	int CREATE2(char v, int tree1, int tree2);
	int CREATE3(char v, int tree1, int tree2, int tree3);
	int ROOT(int tree);
	void MAKENULL(int tree);
	void PRINTNODES();

private:
	NODE nodespace[MAXSIZELOC];
	CELL cellspace[MAXSIZELOC];
};

#endif

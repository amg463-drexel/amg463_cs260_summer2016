#ifndef POINTERQUEUE_H
#define POINTERQUEUE_H
#include "pointerlist.h"
using namespace std;

class POINTERQUEUE {
public:
	POINTERQUEUE();
	
	void MAKENULL();
	int FRONT();
	void ENQUEUE(int x);
	void DEQUEUE();
	bool EMPTY();

private:
	POINTERLIST l;
};

#endif
#include <iostream>
#include "loctree.h"
using namespace std;

//constructor
LOCTREE::LOCTREE() {
}

//param node: cursor to node
//return : cursor to parent of node
int LOCTREE::PARENT(int node) {
	int p = 1;
	int i;
	while (nodespace[p].label != 0) {
		i = nodespace[p].header;
		while (i != 0) {
			if (cellspace[i].node == node) {
				return p;
			}
			i = cellspace[i].next;
		}
		p++;
	}
	return 0;
}

//param node: cursor to node
//return : cursor to leftmost child of node
int LOCTREE::LEFTMOST_CHILD(int node) {
	int child_cell = nodespace[node].header;
	return cellspace[child_cell].node;
}

//param node: cursor to node
//return : cursor to right sibling of node
int LOCTREE::RIGHT_SIBLING(int node) {
	int p = 1;
	int sibling_cell = 0;
	while (cellspace[p].node != 0 && cellspace[p].node != node) {
		p++;
	}
	sibling_cell = cellspace[p].next;
	if (sibling_cell != 0) {
		return cellspace[sibling_cell].node;
	}
	else {
		return 0;
	}
}

//param node: cursor to node
//return : label of node
char LOCTREE::LABEL(int node) {
	return nodespace[node].label;
}

//param v: label for new root
//return : cursor to new root
//creates one node in the cellspace
int LOCTREE::CREATE0(char v) {
	int p = 1;
	while (nodespace[p].label != 0) {
		p++;
	}
	nodespace[p] = NODE(v, 0);
	return p;
}

//param v: label for new root
//param tree 1: cursor to node to be added as subtree of new root
//return : cursor to new root
int LOCTREE::CREATE1(char v, int tree1) {
	int root = CREATE0(v);
	int p = 1;
	while (cellspace[p].node != 0) {
		p++;
	}
	cellspace[p] = CELL(tree1, 0);
	nodespace[root].header = p;
	return root;
}

//param v: label for new root
//param tree 1, tree 2: cursors to nodes to be added as subtrees of new root
//return : cursor to new root
int LOCTREE::CREATE2(char v, int tree1, int tree2) {
	int root = CREATE0(v);
	int p = 1;
	while (cellspace[p].node != 0) {
		p++;
	}
	cellspace[p] = CELL(tree1, p+1);
	cellspace[p + 1] = CELL(tree2, 0);
	nodespace[root].header = p;
	return root;
}

//param v: label for new root
//param tree 1, tree 2, tree 3: cursors to nodes to be added as subtrees of new root
//return : cursor to new root
int LOCTREE::CREATE3(char v, int tree1, int tree2, int tree3) {
	int root = CREATE0(v);
	int p = 1;
	while (cellspace[p].node != 0) {
		p++;
	}
	cellspace[p] = CELL(tree1, p + 1);
	cellspace[p + 1] = CELL(tree2, p+2);
	cellspace[p + 2] = CELL(tree3, 0);
	nodespace[root].header = p;
	return root;
}

//param tree: tree to be made the null node
void LOCTREE::MAKENULL(int tree) {
	nodespace[tree].label = 0;
	nodespace[tree].header = 0;
}

//param tree : cursor to tree
//return : cursor to root of tree
int LOCTREE::ROOT(int tree) {
	return tree;
}

//debugging method
void LOCTREE::PRINTNODES() {
	cout << "NODES: \n";
	int p = 1;
	while (nodespace[p].label != 0) {
		cout << nodespace[p].label << "," << nodespace[p].header << "\n";
		p++;
	}
	p = 1;
	cout << "CELLS: \n";
	while (cellspace[p].node != 0) {
		cout << cellspace[p].node << "," << cellspace[p].next << "\n";
		p++;
	}
}
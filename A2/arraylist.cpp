#include <iostream>
#include "arraylist.h"
using namespace std;

ARRAYLIST::ARRAYLIST() {
	last = 0;
}

int ARRAYLIST::FIRST() {
	return 1;
}

int ARRAYLIST::END() {
	return last + 1;
}

int ARRAYLIST::RETRIEVE(int p) {
	if (p < 1 || p > last) {
		return -1;
	}
	return elements[p];
}

int ARRAYLIST::LOCATE(int x) {
	for (int q = 1; q <= last; q++) {
		if (elements[q] == x) {
			return q;
		}
	}
	return last + 1;
}

int ARRAYLIST::NEXT(int p) {
	if (p < 1 || p > last) {
		return -1;
	}
	return p + 1;
}

int ARRAYLIST::PREVIOUS(int p) {
	if (p <= 1 || p > last + 1) {
		return -1;
	}
	return p - 1;
}

void ARRAYLIST::INSERT(int x, int p) {
	if (last > MAXSIZEALIST) {
		return;
	}
	else if (p < 1 || p > last + 1) {
		return;
	}
	else {
		for (int q = last; q >= p; q--) {
			elements[q + 1] = elements[q];
		}
		last += 1;
		elements[p] = x;
	}
}

void ARRAYLIST::DELETE(int p) {
	if (p < 1 || p > last) {
		return;
	}
	else {
		for (int q = p; q < last; q++) {
			elements[q] = elements[q + 1];
		}
		last -= 1;
	}
}

int ARRAYLIST::MAKENULL() {
	last = 0;
	return 1;
}

void ARRAYLIST::PRINTLIST() {
	for (int q = 1; q <= last; q++) {
		cout << elements[q] << " ";
	}
}
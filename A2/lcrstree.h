#ifndef LCRSTREE_H
#define LCRSTREE_H

#define MAXSIZELCRS 1000

using namespace std;

struct LCRSCELL {
	char label;
	int lchild;
	int rsibling;

	LCRSCELL() {
		label = 0;
		lchild = 0;
		rsibling = 0;
	}

	LCRSCELL(char v, int lc, int rs) {
		label = v;
		lchild = lc;
		rsibling = rs;
	}
};

class LCRSTREE {
public:
	LCRSTREE();
	int PARENT(int node);
	int LEFTMOST_CHILD(int node);
	int RIGHT_SIBLING(int node);
	char LABEL(int node);
	int CREATE0(char v);
	int CREATE1(char v, int tree1);
	int CREATE2(char v, int tree1, int tree2);
	int CREATE3(char v, int tree1, int tree2, int tree3);
	int ROOT(int tree);
	void MAKENULL(int tree);
	void PRINTNODES();

private:
	LCRSCELL cellspace[MAXSIZELCRS];
	int avail;
};

#endif

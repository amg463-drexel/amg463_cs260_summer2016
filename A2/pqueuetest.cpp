#include <iostream>
#include <assert.h>
#include "pointerqueue.h"
#include "pointerlist.h"

using namespace std;

int main(void) {
	POINTERQUEUE Q;

	cout << "QUEUE TEST \n \n";

	//test EMPTY and constructor
	assert(Q.EMPTY() == true);
	//--------------------------------------------------
	//--------test procedures with side effects---------
	//--------------------------------------------------
	Q.ENQUEUE(5);
	assert(Q.FRONT() == 5);
	Q.ENQUEUE(6);
	assert(Q.FRONT() == 5);
	Q.DEQUEUE();
	assert(Q.FRONT() == 6);

	//test MAKENULL
	assert(Q.EMPTY() == false);
	Q.MAKENULL();
	assert(Q.EMPTY() == true);

	//test error cases
	assert(Q.FRONT() == -1);
	Q.DEQUEUE();
	assert(Q.FRONT() == -1);

	cout << "PASSED ALL TESTS \n";

	return 0;
}

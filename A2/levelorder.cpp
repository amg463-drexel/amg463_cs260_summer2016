#include <iostream>
#include "lcrstree.h"
#include "pointerqueue.h"

using namespace std;

//param root: the root of the tree
//param t: the LCRSTREE object which contains tree with root
//traverses a tree in level-order, printing each node as it goes
void levelOrderTraverse(int root, LCRSTREE t) {
	cout << "\n level order traversal of tree 3.31 is: \n";
	if (root == 0) {
		return;
	}
	POINTERQUEUE q;
	q.ENQUEUE(root);

	while (!q.EMPTY()) {
		int node = q.FRONT();
		char label = t.LABEL(node);
		cout << label << " ";
		q.DEQUEUE();

		int child = t.LEFTMOST_CHILD(node);
		while (child != 0) {
			q.ENQUEUE(child);
			child = t.RIGHT_SIBLING(child);
		}
	}
}

int main(void) {
	LCRSTREE t;

	//construct tree 3.31
	cout << "\n constructing tree 3.31 with nodes a through n \n";
	int m = t.CREATE0('m');
	int n = t.CREATE0('n');
	int i = t.CREATE2('i', m, n);
	int e = t.CREATE1('e', i);
	int d = t.CREATE0('d');
	int b = t.CREATE2('b', d, e);
	int j = t.CREATE0('j');
	int k = t.CREATE0('k');
	int l = t.CREATE0('l');
	int g = t.CREATE2('g', j, k);
	int h = t.CREATE1('h', l);
	int f = t.CREATE0('f');
	int c = t.CREATE3('c', f, g, h);
	int a = t.CREATE2('a', b, c);

	levelOrderTraverse(a, t);
	cout << "\n";
	return 0;
}